import pip

def install(package):
    pip.main(['install', package])

    
if __name__ == '__main__':
    install('matplotlib')
    install("numpy")
    install("scipy")
    install("graphyte")
    install("sht_sensor")
    install("pyserial")
    install("zmq")
    install("Pi-Plates")
    try:
        install("configparser")
    except:
        install("ConfigParser")
