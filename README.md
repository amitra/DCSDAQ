# ColdJig Virtual Instruments Setup

1. Make sure you have Python 3 installed
2. Clone the repo and checkout the V0.1 tag into a V0.1 branch
	- git clone ssh://git@gitlab.cern.ch:7999/amitra/DCSDAQ.git
	- cd DCSDAQ
	- git fetch --all --tags --prune
	- git checkout tags/V0.1 -b V0.1
3. Create a virtual environment (to avoid touching your system python)
	- python3 -m venv env
	- source env/bin/activate
4. Install packages defined in requirements.txt
	- pip3 install -r requirements.txt
5. python3 GUIroute.py 
	- at login, the password is ‘chiller’
	- password can be changed with python3 set_password.py’



