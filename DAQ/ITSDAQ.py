import subprocess
from subprocess import Popen, PIPE
import time
import os
from six.moves import input
from zmq_client import Client
#subprocess.call(["ls", "-l"])

def hists():
    p = Popen(['root',"-l","summary.root"],stdin=PIPE)
    p.stdin.write('A6Q2XKH_089->Draw()\n'.encode())
    p.stdin.write('c1->Print("FromPython.pdf")\n'.encode())


def test():
	p = Popen(['root'],stdin=PIPE)
	p.stdin.write(".x test.C\n".encode())
	p.stdin.write(".x test2.C\n".encode())
	p.stdin.write(".q\n".encode())
	p.stdin.write("\n".encode())
	p.communicate(".q".encode())
	master=Client("127.0.0.1","5554")
	master.SendServerMessage("finished test")

def batchMode():
    #subprocess.call(["root", "-l","-q", "-b"," test.C"])
    os.system("root -b -l -q  test.C ")

def StrobeDelay():
    master=Client("127.0.0.1","5554")

    '''
    username=input("Please enter the username you wish to use for DB upload: ")
    while True:
        reply=input("is "+str(username) +" ok? [yes/no] ")
        if "yes" in reply:
            break
        else:
            username=input("Please enter the username you wish to use for DB upload: ")
    '''


    username='guy'
    with open("macros/userfile.txt",'w') as uf:
        uf.write(username)
        
    p = Popen(['root',"-l"],stdin=PIPE)
    p.stdin.write(".x Stavelet.cpp\n".encode())

    p.stdin.write(".L CaptureWhateverABC130.cpp\n".encode())
    p.stdin.write("CaptureABC130_HCC_Pattern()\n".encode())
    p.stdin.write("CaptureABC130Chips(2048+50)\n".encode())
    p.stdin.write("gROOT->ProcessLine(\".L ABC130StrobeDelay.cpp\")\n".encode())
    p.stdin.write("gROOT->ProcessLine(\".L ABC130StrobeDelayPlot.cpp\")\n".encode())
    p.stdin.write("ABC130StrobeDelay(ST_VTHR, 0.57)\n".encode())
    #p.stdin.write(".x ABC130_GuyTest.cpp\n".encode())
    p.stdin.write("e->HVOff()\n".encode())
    p.stdin.write("e->LVOff()\n".encode())
    p.communicate(".q".encode())
    #os.system(r"rm userfile.txt")
    master.SendServerMessage("Finished Running Strobe delay")
    
def StartAmac():
    p = Popen(['root',"-l"],stdin=PIPE)
    p.stdin.write(".x Stavelet.cpp\n".encode())
    p.stdin.write("e->AmacWriteConfig(-1)\n".encode())
    p.stdin.write("e->AmacReadAnalogue(-1)\n".encode())
    p.stdin.write("e->LVOn()\n".encode())
    p.stdin.write("e->HVOn()\n".encode())
    p.stdin.write(".L CaptureWhateverABC130.cpp\n".encode())
    p.stdin.write("CaptureABC130_HCC_Pattern()\n".encode())
    p.stdin.write("CaptureABC130Chips(2048+50)\n".encode())
    p.communicate(".q".encode())

def StopAmac():
    p = Popen(['root',"-l"],stdin=PIPE)
    p.stdin.write(".x Stavelet.cpp\n".encode())
    p.stdin.write("e->AmacWriteConfig(-1)\n".encode())
    p.stdin.write("e->AmacReadAnalogue(-1)\n".encode())
    p.stdin.write("e->HVOff()\n".encode())
    p.stdin.write("e->LVOff()\n".encode())
    p.communicate(".q".encode())

if __name__=="__main__":
	test()

    
