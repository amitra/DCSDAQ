import zmq
import time
import sys
import threading
import ITSDAQ 
from zmq_client import Client

class ServerThread(threading.Thread):
	def __init__(self,threadID):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.lock = threading.Lock()

	def run(self):
		print("Starting " + str(self.threadID))
		self.server()

	def server(self):
		port = "5555"
		context = zmq.Context()
		socket=context.socket(zmq.REP)
		socket.bind("tcp://*:%s" %port)
		while True:
			message = str(socket.recv())
			print( "recieved message: ",message)
			if "break" in message.lower():
				socket.send("Goodbye!".encode())
				break
			elif "strobe" in message:
				ITSDAQ.StrobeDelay()
				socket.send("Running Strobe delay".encode())
                        elif "start_amac" in message.lower():
                                ITSDAQ.StartAmac()
				socket.send("Runing Amac".encode())
                        elif "stop_amac" in message.lower():
                                ITSDAQ.StopAmac()
				socket.send("stopping Amac".encode())

			elif "test" in message.lower():
				socket.send("Running a test".encode())
				ITSDAQ.StrobDelay()
			else:
				socket.send("Good to go!".encode())

Thread=ServerThread("ITS server")
Thread.start()
