import piplates.DAQCplate as DAQ
import math
from Sensor import Sensor
class NTC(Sensor):
	def __init__(self,input_pin=0,resistance=10000,B=3971):
		Sensor.__init__(self,"NTC")
		self.input_pin=int(input_pin)
		self.Ro=float(resistance)
		self.B=float(B)
		
	def Temperature(self):
                print("input pin ",self.input_pin)
		Vin=DAQ.getADC(0,self.input_pin)
                To=298.0  #To of NTC
		Ri=self.Ro*math.exp(-self.B/To)
		Rfx=10000.0
    
		Rn=(Vin*Rfx)/(5-Vin)
		T=self.B/(math.log(Rn/Ri))-273
		return T

	def get_data(self):
                return {"temperature":self.Temperature()}
        
def Temperature():
	Ro=10000
	B=3971
	input_pin=0
	Vin=DAQ.getADC(0,0)
	To=298.0  #To of NTC
	Ri=Ro*math.exp(-B/To)
	Rfx=10000.0
    
	Rn=(Vin*Rfx)/(5-Vin)
	T=B/(math.log(Rn/Ri))-273
	return T
