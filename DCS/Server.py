import time
import graphyte
import zmq
import threading
import serial.tools.list_ports as ports
from zmq_client import Client
import re

#local configuration
from initialize import Chiller
from initialize import HV
from initialize import LV
from initialize import SensorDict


try:
	import queue
except ImportError:
	import Queue as queue

from initialize import HighVoltage,LowVoltage,Chill


class ServerThread(threading.Thread):
	def __init__(self,threadID, queue, HV=None, LV=None,Chiller=None):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.queue=queue
		self.chiller = Chiller
		self.lock = threading.Lock()
		self.HighVoltage = HV
		self.LowVoltage = LV

	def run(self):
		print("Starting " + str(self.threadID))
		self.server()

	def parse_chiller(self,message):
		try:
			temp = float(re.findall(r"-*\d+\.*\d*",message)[0])
		except IndexError:
			print("tried to set temperature but failed")
			return "failed to talk to chiller"
		with self.lock:
			print("setting temperature to",temp)
			try:
				self.chiller.SetTemperature(temp)
				return "set chiller temp to "+str(temp)
			except AttributeError:
				return "failed to talk to chiller"

		return "failed to talk to chiller"

	def parse_HV(self,message):
		data=re.findall(r"-*\d+\.*\d*",message)
		attr=float(data[0])
		try:
			channel=int(data[1])
		except IndexError:
			channel=1
			reply=""
			try:
				if 'on' in message:
					self.HV.TurnOn()
				reply="turned on"
			if 'off' in message.lower():
				self.HV.TurnOff()
				reply= "turned off"
			with self.lock:
				if('volt' in message.lower()):
					if("protection" in message):
						self.HV.SetVProtection(attr,channel)
						reply= "voltage protection set" +str(attr)
					else:
						self.HV.SetVoltage(attr,channel)
						reply= "voltage  set" +str(attr)
				else:
					if("protection" in message):
						self.HV.SetVProtection(attr,channel)
						reply= "current protection set" +str(attr)
					else:
						self.HV.SetVoltage(attr,channel)
						reply= "current set" +str(attr)
                except AttributeError:
                        reply= "Failed to Talk To high voltage"
                return reply

	def parse_LV(self,message):
		data=re.findall(r"-*\d+\.*\d*",message)
		attr=float(data[0])
		try:
			channel=int(data[1])
		except IndexError:
			channel=1
			if 'on' in message.lower():
				self.LV.TurnOn()
				return "turned on"
			if 'off' in message.lower():
				self.LV.TurnOff()
				return "turned off"
			with self.lock:
				if('volt' in message.lower()):
					if("protection" in message):
						self.LV.SetVProtection(attr,channel)
						return "voltage protection set" +str(attr)
					else:
						self.LV.SetVoltage(attr,channel)
						return "voltage set"+str(attr)
				else:
					if("protection" in message):
						self.LV.SetVProtection(attr,channel)
						return "current protection set" +str(attr)
					else:
						self.LV.SetVoltage(attr,channel)
						return "current set" +str(attr)

	def start_sensors(self):
		sensor_thread=SensorThread("sensor thread",self.queue,Chiller=self.chiller)
		Thread2.start()

	def server(self):
		port = "5556"
		context = zmq.Context()
		socket=context.socket(zmq.REP)
		socket.bind("tcp://*:%s" %port)
		while True:
			message = socket.recv()
			print ("recieved message: ",message)
			if "break" in message.lower():
				socket.send("Goodbye!")
				self.queue.put("break")
				break

			if "read" in message.lower():
				try:
					channel=int(re.findall(r"\d+",message)[0])
				except IndexError:
					channel=1
					HV_volt=self.HV.ReadVoltage(channel)
					HV_current=self.LV.ReadVoltage(channel)
					LV_volt=self.LV.ReadVoltage(channel)
					LV_current=self.LV.ReadVoltage(channel)
					chiller_temp=self.chiller.GetTemperature()
					messsage ="HV: Voltage:"+str(HV_volt) +" Current:"+str(HV_current)
					message+= "LV: Voltage:"+str(LV_volt) +" Current:"+str(LV_current)
					socket.send(message)

			elif("chiller" in message.lower()):
				result=self.parse_chiller(message)
				socket.send(result.encode())
			elif("high" in message.lower() and "volt" in message.lower()):
				result=self.parse_HV(message)
				socket.send(result.encode())
			elif("low" in message.lower() and "volt" in message.lower()):
				result=self.parse_LV(message)
				socket.send(result.encode())
			elif('sensor' in message.lower()):
				if('on' in message.lower()):
					start_sensors()
					socket.send("Started Data Taking".encode())
				else:
					self.queue.put('break')
					socket.send("stopped data taking".encode())
			else:
				socket.send("good to go")

class SensorThread(threading.Thread):
    def __init__(self,threadID,comm_queue, grafana="10.2.228.58",Chiller=None):
	threading.Thread.__init__(self)
	self.threadID = threadID
	self.chiller = Chiller
	self.queue=comm_queue
	self.lock = threading.Lock()
	graphyte.init(grafana)
	self.MasterServer=Client("10.2.244.83","5554")
        self.setup="one module box"

    def run(self):
	print ("Starting " + str(self.threadID))
	self.Read()

    def Read(self):
	command=""
	while True:
	    if (not self.queue.empty()):
		command=str(self.queue.get())
		print("from queue", command)
		if 'break' in command.lower():
		    break
	    with self.lock:
		try:
		    chiller_t = self.chiller.GetTemperature()
		    graphyte.send(self.setup+".Chiller_Temperature",float(chiller_t))
		except AttributeError:
		    print("chiller not responding")
	    for label,sensor in SensorDict.items():

			grafana_name='OneModule.'+str(label)
			for name,data in sensor.get_data:
				grafana_name+="."+name
				print(grafana_name,datat)
				graphyte.send(grafana_name,float(data))
				if('chuck' in label.lower()):
					try:
						self.MasterServer.SendServerMessage("NTC temperature: "+str(data))
					except IOError:
						print("Master server not responding")
            time.sleep(5)


if __name__=="__main__":
	CommunicationQueue=queue.Queue()
	Thread1=ServerThread("Server Thread",CommunicationQueue,HV=HighVoltage,LV=LowVoltage,Chiller=Chill)
	Thread1.start()
	Thread2=SensorThread("sensor thread",CommunicationQueue,Chiller=Chill)
	Thread2.start()
