import imp
import os
import LV
import Interlock
import HV
import serial.tools.list_ports
import Sensors
import re
from SerialDevice import SerialDevice


#list all of the classes in a package
def package_contents(package_name):
    MODULE_EXTENSIONS = ('.py', '.pyc', '.pyo')
    
    file, pathname, description = imp.find_module(package_name)
    if file:
        raise ImportError('Not a package: %r', package_name)
    # Use a set because some may be both source and compiled.
    return set([os.path.splitext(module)[0]
                for module in os.listdir(pathname)
                if (module.endswith(MODULE_EXTENSIONS) and not "__init__" in module.lower() and not "tool" in module.lower() and not module.lower()=="Sensor" ) ])

#takes the name of a package(string such as 'LV') and name of class ('Sorensen') and returns an instance of that class  
def init_class(package,obj_name,*args):
    module = __import__(package)
    DeviceType = getattr(module,obj_name )
    device=DeviceType(*args)
    return device

#same deal, returns an instance of the class specifice by obj_name, but arguements are given as a dictionary
def init_class_with_dict(package,obj_name,dictionary):
    module = __import__(package)
    print(obj_name)
    DeviceType = getattr(module,obj_name )
    device=DeviceType(**dictionary)
    return device

#list avaible comports and try to guess what device is on what port 
def list_devices():
    devices =serial.tools.list_ports.comports(True)
    deviceList = {}
    if not devices:
        return None
    for i in devices:
        dev=str(i)
        path=dev.split()[0]
        device=SerialDevice(location=path)
        name=device.GetType()
        deviceList[path]=name
    return deviceList

#takes a package name, such as HV,LV find the com port that its on and what type it is (sorensen vs. instek) and return an instance of that type (under development)
def init_device(device_type):
    '''device type = HV, LV aybe chiller if it can *idn? '''
    list_of_types=package_contents(device_type)
    deviceList=list_devices()
    try:
        print(device)
        for path,name in deviceList.items():
            for device in list_of_types:
                if device.lower() in name.lower():
                    print("initlized "+str(device)+" in "+str(path))
                    return init_class(device_type,device,path)
    except:
        return None
	        
#take an object instance, an object level method and the arguements for it and execute that method with the given arguements.
#example: evaluate(kethely,turnOn) turns on keithley 
def evaluate(obj,method,data=[]):
    #data should be a list [] of arguements
    try:
        function=getattr(obj,method)
        reply=''
        if(data):
            reply=function(*data)
        else:
             reply=function()
             reply=str(reply)
             
        if(reply=='' or reply==None):
            return str(method)+" success"
        else:
            return reply
    except:
        return str(method)+ "failed"

#tree structure for  parsing, take a message from server and determine what to do, message shoudl have format device,action,[sub_action],data
#example lowv,set,voltage, 10  volts channel 1
class message_parser:
    
    def __init__(self,HV,LV,chiller,interlock):
       hv_attributes={
           "turn":{"off":"TurnOff",
                   "on":"TurnOn"},
           "voltage":{
               'protection':
               {'set':"SetVProtection",'get':"SetVoltageCompliance"},
               "set":"SetVoltageLevel",
               "get":"GetVoltage"},
           "current":{
               'protection':
               {'set':"SetIProtection",'get':"SetCurrentCompliance"},
               "set":"SetCurrentLevel",
               "get":"GetCurrent"}} 

       lv_attributes={
           "turn":{"off":"TurnOff",
                   "on":"TurnOn"},
           "voltage":{
               'protection':
               {'set':"SetVProtection",'get':"GetVProtection"},
               "set":"SetVoltage",
               "get":"GetVoltage"},
           "current":{
               'protection':
               {'set':"SetIProtection",'get':"GetIProtection"},
               "set":"SetCurrent",
               "get":"GetActualCurrent"}} 

       chiller_attributes={"set":"SetTemperature",
                           "get":"GetTemperature"}
       
       interlock_attributes={
           "sht":{
               "temp":"set_SHT_temp",
               "humidity":"set_humidity_range"},
           "user ntc":"set_user_NTC_temp",
           "hybrid ntc":"set_hybdrid_NTC_temp"}
       self.HV=HV
       self.LV=LV
       self.Chiller=chiller
       self.Interlock=interlock
    
       self.dcs_dict={"highv":(self.HV,hv_attributes),
                      "lowv":(self.LV,lv_attributes),
                      "interlock":(self.Interlock,interlock_attributes),
                      "chiller":(self.Chiller,chiller_attributes)}




    
    def parse(self,message):
        data=re.findall(r"-*\d+\.*\d*",message)
        message=message.lower()
        for i in range(len(data)):
            data[i]=float(data[i])
        try:
            words=message.split(",")
            system=words[0]
            word_list=words[1:]
            obj=self.dcs_dict[system][0]
            methods=self.dcs_dict[system][1]
            result=methods
        except KeyError:
            return "Nothing to be done"
        for word in word_list:
            try:
                result=result[word]
            except (KeyError,TypeError):
                break
        print("in praser")
        print(system,result,data)
        reply=evaluate(obj,result,data)
        return reply



