try:
    import configparser
except ImportError:
    import ConfigParser as configparser

import LV
import HV
import Chiller
import Interlock
from Sensors import *

import DCSTools as fm

def DCSConfig():
    parser = configparser.ConfigParser()
    parser.read("DCSConfig.ini")
    sections=parser.sections()
    dcs_dict={}
    for section in sections:
        if('HighVoltage' in section):
            HV_type=parser['HighVoltage']['Type']
            HV_location=parser['HighVoltage']['Location']
            dcs_dict['HV']=fm.init_class("HV",HV_type,HV_location)
        if('LowVoltage' in section):            
            LV_type=parser['LowVoltage']['Type']
            LV_location=parser['LowVoltage']['Location']
            dcs_dict['LV']=fm.init_class("LV",LV_type,LV_location)
        if('Chiller' in section):
            Chiller_type=parser['Chiller']['Type']
            Chiller_location=parser['Chiller']['Location']
            dcs_dict['Chiller']=fm.init_class("Chiller",Chiller_type,Chiller_location)
            
        if('Interlock' in section):
            interlock_location=parser['Interlock']['location']
            interlock_baud=parser['Interlock']['baud']
            interlock=Interlock.Cambridge(interlock_location,interlock_baud)    
    return dcs_dict 

def MasterConfig():
    parser = configparser.ConfigParser()
    parser.read("DCSConfig.ini")
    master_server=str(parser['MasterServer']['location'])
    grafana_server=str(parser['MasterServer']['grafana'])
    return(master_server,grafana_server)

def IVCurveConfig():
    parser=configparser.ConfigParser()
    parser.read("DCSConfig.ini")
    step=int(str(parser['IVCurve']['step']))
    start=int(str(parser['IVCurve']['start']))
    end=int(str(parser['IVCurve']['stop']))
    return (step,end,start)

def SensorConfig():
    parser = configparser.ConfigParser()
    parser.read("SensorConfig.ini")
    sensor_package=fm.package_contents("Sensors")
    sections=parser.sections()
    sensors={}
    for section in sections:
        for sensor_name in sensor_package:
            if sensor_name in section:
                args=parser._sections[section]
                print(section)
                sensor_type=fm.init_class_with_dict("Sensors",sensor_name,args)
                sensors[section]=sensor_type
           
    return sensors

dcs_dict=DCSConfig()
SensorDict=SensorConfig()
master,grafana=MasterConfig()

