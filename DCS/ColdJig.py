from __future__ import print_function
import time
import graphyte
import zmq
import threading
import thread
from zmq_client import Client
import re
import sht_sensor
try:
    import queue
except ImportError:
    import Queue as queue

from DCSTools import message_parser


#local configuration
from initialize import SensorDict
from initialize import master,grafana
from initialize import dcs_dict
from initialize import IVCurveConfig
import HVTools

class ServerThread(threading.Thread):
    def __init__(self,threadID, CommQueue, HV=None, LV=None,Chiller=None,interlock=None):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.queue=CommQueue
        self.chiller = Chiller
        self.lock = threading.Lock()
        self.interlock=interlock
        self.HV = HV
        self.LV = LV
        self.coldjig_parser=message_parser(HV,LV,Chiller,interlock)
        
    def run(self):
        print("Starting " + str(self.threadID))
        self.server()



    def start_sensors(self):
        sensor_thread=SensorThread("sensor thread",self.queue,Chiller=self.chiller,interlock=self.interlock)
        sensor_thread.start()

    def server(self):
        port = "5556"
        context = zmq.Context()
        socket=context.socket(zmq.REP)
        socket.bind("tcp://*:%s" %port)
        while True:
            message = socket.recv()
            print ("recieved message: ",message)
            #exit both server loop and data taking loop
            if "break" in message.lower():
                socket.send("Goodbye!")
                self.queue.put("break")
                break
                
            elif('iv curve' in message.lower()):
                #run IV curve test in seperate thread
                step,end,start=IVCurveConfig()
                thread.start_new_thread(HVTools.IVCurve, (self.HV,step,end,start,))
                socket.send("IV done")
                continue
            elif('sensor' in message.lower()):
                if('on' in message.lower()):
                    self.queue.queue.clear()
                    self.start_sensors()
                    socket.send("Started Data Taking".encode())
                else:
                    self.queue.put('break')
                    socket.send("stopped data taking".encode())
            else:
                try:
                    reply=self.coldjig_parser.parse(message)
                    socket.send(reply.encode())
                except:
                    reply="order: "+str(message)+" : failed"
                    socket.send(reply)


                            
class SensorThread(threading.Thread):
    def __init__(self,threadID,comm_queue,Chiller=None,interlock=None):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.chiller = Chiller
        self.queue=comm_queue
        self.lock = threading.Lock()
        self.interlock=interlock
        global master
        global grafana
        graphyte.init(grafana)
        self.MasterServer=Client(master,"5554")
    

    def run(self):
        print ("Starting " + str(self.threadID))
        self.Read()

    def Read(self):
        command=""
        while True:
            if (not self.queue.empty()):
                command=str(self.queue.get())
                print("from queue", command)
                if 'break' in command.lower():
                    break
            with self.lock:
                try:
                    chiller_t = self.chiller.GetTemperature()
                    graphyte.send("OneModule.Chiller_Temperature",float(chiller_t))
                except :
                    print("chiller not responding")

            with self.lock:
                try:
                    data=self.interlock.get_data()
                    data=data.split(",")
                    metrics=["hybrid NTC 1",
                             "hybrid NTC 2",
                             "hybrid NTC 3",
                             "hybrid NTC 4",
                             "hybrid NTC 5",
                             "hybrid NTC 6",
                             "hybrid NTC 7",
                             "hybrid NTC 8",
                             "user NTC 1",
                             "user NTC 2",
                             "SHT_temp",
                             "SHT_humidity"]
                    
                    for point,metric in zip(data,metrics):
                        point=int(point)
                        name="interlock."+metric
                        graphyte.send(name,point)
                except AttributeError:
                    print("interlock not responding")
                    
            for label,sensor in SensorDict.items():
                try:
                    sensor_items=sensor.get_data().items()
                except:
                    print("problem with "+label)
                    continue
                for name,data in sensor_items:
                    try:
                        grafana_name='OneModule.'+str(label)
                        grafana_name+="."+name
                        print(grafana_name,data)
                        graphyte.send(grafana_name,float(data))
                        if('chuck' in label.lower()):
                            try: #send NTC temp to master server to run module tests
                                self.MasterServer.SendServerMessage("NTC temperature for: "+str(label)+" "+str(data))
                            except IOError:
                                print("Master server not responding")
                    except (sht_sensor.sensor.ShtCRCCheckError,sht_sensor.sensor.ShtCommFailure):
                        print("SHT sensor in config file but not connected properly")
                    except AttributeError:
                        print("encountered a problem with "+label)
            time.sleep(5)


if __name__=="__main__":
	CommunicationQueue=queue.Queue()
	Thread1=ServerThread("Server Thread",CommunicationQueue,**dcs_dict)
	Thread1.start()
